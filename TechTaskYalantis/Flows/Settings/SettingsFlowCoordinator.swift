//
//  SettingsFlowCoordinator.swift
//  TechTaskYalantis
//
//  Created by Liudmila on 19.12.2021.
//

import UIKit

public class SettingsFlowCoordinator: NavigationNode, FlowCoordinator {

    var containerViewController: UIViewController?

    func createFlow() -> UIViewController {
        let dataBaseService = DataBaseService()
        let settingsViewModel = SettingsViewModel(settingsModel: SettingsModel(dataBaseProvider: dataBaseService))
        return SettingsViewController(settingsViewModel: settingsViewModel)
    }

}
