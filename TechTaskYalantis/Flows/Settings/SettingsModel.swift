//
//  SettingsModel.swift
//  TechTaskYalantis
//
//  Created by Liudmila on 17.11.2021.
//

import Foundation

class SettingsModel {

    private let dataBaseProvider: DataBaseProvider

    init(dataBaseProvider: DataBaseProvider) {
        self.dataBaseProvider = dataBaseProvider
    }

    func saveCustomAnswer(answer: String?) {
        guard let answer = answer else { return }
        dataBaseProvider.saveAnswer(answer: Answer(answer: answer, date: Date(), isLocal: true))
    }
}
