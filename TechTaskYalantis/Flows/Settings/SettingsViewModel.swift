//
//  SettingsViewModel.swift
//  TechTaskYalantis
//
//  Created by Liudmila on 17.11.2021.
//

import Foundation

class SettingsViewModel {

    private let settingsModel: SettingsModel

    init(settingsModel: SettingsModel) {
        self.settingsModel = settingsModel
    }

    func saveNewCustomAnswer(answer: String) {
        settingsModel.saveCustomAnswer(answer: answer)
    }

}
