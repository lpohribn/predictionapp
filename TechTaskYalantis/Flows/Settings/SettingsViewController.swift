//
//  SettingsViewController.swift
//  TechTaskYalantis
//
//  Created by Liudmila on 21.10.2021.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa

class SettingsViewController: UIViewController {

    private let answerLabel = UILabel()
    private let answerTextField = UITextField()
    private let backgroundImageView = UIImageView()
    private let saveAnswerButton = UIButton()

    private let settingsViewModel: SettingsViewModel

    init(settingsViewModel: SettingsViewModel) {
        self.settingsViewModel = settingsViewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }

    func setupUI() {
        view.backgroundColor = UITraitCollection.current.userInterfaceStyle == .dark ? UIColor(named: .black) : UIColor(named: .white)
        setupNavigationBar()
        setupBackground()
        setTabBar()
        setupAnswerLabel()
        setupAnswerTexfield()
        setupSaveButton()
    }

    private func showMessage(text: String) {
        let alert = UIAlertController(title: nil, message: text, preferredStyle: .alert)
        self.present(alert, animated: true, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.7) {
            alert.dismiss(animated: true, completion: nil)
        }
    }

}

extension SettingsViewController: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        answerTextField.resignFirstResponder()
    }

}

extension SettingsViewController {

    private func setupNavigationBar() {
        if let navBar = navigationController?.navigationBar {
            navigationItem.title = "Settings"
            navBar.backgroundColor = UITraitCollection.current.userInterfaceStyle == .dark ? UIColor(named: .black) : UIColor(named: .white)
            navBar.titleTextAttributes = UITraitCollection.current.userInterfaceStyle == .dark ? [.foregroundColor: UIColor(named: .white)] :
            [.foregroundColor: UIColor(named: .black)]
        }
    }

    private func setupBackground() {
        backgroundImageView.image = Asset.nightSkyPines.image
        view.addSubview(backgroundImageView)
        backgroundImageView.snp.makeConstraints { make in
            make.top.left.right.bottom.equalTo(view.safeAreaLayoutGuide)
            make.bottom.equalToSuperview()
        }
    }

    private func setupAnswerLabel() {
        answerLabel.textColor = .white
        answerLabel.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
        answerLabel.text = L10n.settingsLabel
        answerLabel.textAlignment = .left
        view.addSubview(answerLabel)
        answerLabel.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top).inset(40)
            make.left.right.equalTo(view.safeAreaLayoutGuide).inset(30)
        }
    }

    private func setupAnswerTexfield() {
        answerTextField.placeholder = L10n.titleForPlaceHolder
        answerTextField.font = UIFont.systemFont(ofSize: 15)
        answerTextField.borderStyle = UITextField.BorderStyle.roundedRect
        answerTextField.autocorrectionType = UITextAutocorrectionType.no
        answerTextField.keyboardType = UIKeyboardType.default
        answerTextField.returnKeyType = UIReturnKeyType.done
        answerTextField.clearButtonMode = UITextField.ViewMode.whileEditing
        answerTextField.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        answerTextField.delegate = self
        view.addSubview(answerTextField)
        answerTextField.snp.makeConstraints { make in
            make.top.equalTo(answerLabel.snp.bottom).inset(-15)
            make.left.right.equalTo(view.safeAreaLayoutGuide).inset(30)
        }
    }

    private func setTabBar() {
        if let tabBar = tabBarController?.tabBar {
            tabBar.backgroundColor = .white
        }
    }

    private func setupSaveButton() {
        saveAnswerButton.backgroundColor = UIColor(named: .lightPurple)
        saveAnswerButton.layer.cornerRadius = 4
        saveAnswerButton.setTitle(L10n.saveButtonText, for: .normal)
        saveAnswerButton.addTarget(self, action: #selector(saveCustomAnswerButtonPressed), for: .touchUpInside)
        view.addSubview(saveAnswerButton)
        saveAnswerButton.snp.makeConstraints { make in
            make.right.left.equalTo(view.safeAreaLayoutGuide).inset(30)
            make.height.equalTo(40)
            make.bottom.equalTo(view.safeAreaLayoutGuide).inset(100)
        }
    }

    @objc func saveCustomAnswerButtonPressed() {
        animateSaveButton()
        if answerTextField.text != "" {
            showMessage(text: L10n.save)
            settingsViewModel.saveNewCustomAnswer(answer: answerTextField.text ?? "")
            answerTextField.text = ""
        } else {
            showMessage(text: L10n.warningForField)
        }
    }

    private func animateSaveButton() {
        saveAnswerButton.backgroundColor = .gray
        UIView.animate(withDuration: 0.1,
                       delay: 0,
                       options: [],
                       animations: { [weak self] in
            self?.saveAnswerButton.transform = CGAffineTransform.init(scaleX: 0.95, y: 0.95)
            self?.saveAnswerButton.backgroundColor = UIColor(named: .lightPurple)
        }, completion: {  _ in
            UIView.animate(withDuration: 0.1,
                           delay: 0,
                           options: [],
                           animations: { [weak self] in
                self?.saveAnswerButton.transform = CGAffineTransform.init(scaleX: 1, y: 1)
            },
                           completion: nil
            )
        })

    }
}
