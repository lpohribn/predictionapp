//
//  TabBarViewController.swift
//  TechTaskYalantis
//
//  Created by Liudmila on 25.11.2021.
//

import UIKit

class TabBarViewController: UITabBarController {

    init(viewControllers: [UIViewController]) {
        super.init(nibName: nil, bundle: nil)
        setViewControllers(viewControllers, animated: true)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
