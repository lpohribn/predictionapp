//
//  TabBarFlowCoordinator.swift
//  TechTaskYalantis
//
//  Created by Liudmila on 19.12.2021.
//

import UIKit

public class TabBarFlowCoordinator: NavigationNode, FlowCoordinator {

    var containerViewController: UIViewController?
    private let networkManager = NetworkingManager()
    private let dataBaseService = DataBaseService()
    private let secureStorage = SecureStorage()

    override init(parent: NavigationNode?) {
        super.init(parent: parent)
    }

    func createFlow() -> UIViewController {
        let mainViewController = MainFlowCoordinator(parent: self,
                                                     networkManager: networkManager,
                                                     dataBaseService: dataBaseService,
                                                     secureStorage: secureStorage).createFlow()
        mainViewController.tabBarItem = UITabBarItem(title: "Main", image: UIImage(systemName: "circle"), tag: 0)
        let historyViewController = HistoryFlowCoordinator(parent: self, dataBaseService: self.dataBaseService).createFlow()
        historyViewController.tabBarItem = UITabBarItem(title: "History", image: UIImage(systemName: "list.dash"), tag: 1)
        return TabBarViewController(viewControllers: [mainViewController, historyViewController])
    }
}
