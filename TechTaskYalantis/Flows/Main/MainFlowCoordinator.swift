//
//  MainFlowCoordinator.swift
//  TechTaskYalantis
//
//  Created by Liudmila on 19.12.2021.
//

import UIKit

enum MainScreenEvent: NavigationEvent {
    case settings
}

public class MainFlowCoordinator: NavigationNode, FlowCoordinator {

    var containerViewController: UIViewController?
    private let networkManager: NetworkingManager
    private let dataBaseService: DataBaseService
    private let secureStorage: SecureStorage

    init(parent: NavigationNode?, networkManager: NetworkingManager, dataBaseService: DataBaseService, secureStorage: SecureStorage) {
        self.networkManager = networkManager
        self.dataBaseService = dataBaseService
        self.secureStorage = secureStorage
        super.init(parent: parent)
        addHandlers()
    }

    private func addHandlers() {
        addHandler { [weak self] (event: MainScreenEvent) in
            guard let self = self else { return }

            switch event {
            case .settings:
                self.goToSettings()
            }
        }
    }

    private func goToSettings() {
        let settingsVC = SettingsFlowCoordinator(parent: self).createFlow()
        containerViewController?.navigationController?.pushViewController(settingsVC, animated: true)
    }

    func createFlow() -> UIViewController {
        let mainModel = MainModel(parent: self, networkDataProvider: networkManager, dataBaseProvider: dataBaseService, secureProvider: secureStorage)
        let mainViewModel = MainViewModel(mainModel: mainModel)
        containerViewController = MainViewController(mainViewModel: mainViewModel)
        let navigationController = UINavigationController(rootViewController: containerViewController ?? UIViewController())
        return navigationController
    }

}
