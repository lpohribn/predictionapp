//
//  MainViewModel.swift
//  TechTaskYalantis
//
//  Created by Liudmila on 14.11.2021.
//

import Foundation
import RxSwift

class MainViewModel {

    private let mainModel: MainModel

    private let disposeBag = DisposeBag()

    init(mainModel: MainModel) {
        self.mainModel = mainModel
    }

    func updateViewPresenting() -> Observable<String?> {
        mainModel.fetchAnswer()
            .map { $0?.uppercased() }
        }

    func updateCountOfShake(count: Int) {
        mainModel.saveCountOfShake(count: count)
    }

    func presentCurrentCountOfShake() -> Int {
        return mainModel.getCountOfShake()
    }

    func saveHistoryAnswer(answer: String) {
        mainModel.saveAnswer(answer: answer)
    }

    func goToSettings() {
        mainModel.goToSettings()
    }
}
