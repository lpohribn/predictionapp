//
//  MagicBallView.swift
//  TechTaskYalantis
//
//  Created by Liudmila on 04.12.2021.
//

import UIKit

class MagicBallView: UIView {

    private let screenView: UIView = UIView()
    private let predictionLabel: UILabel = UILabel()
    private let gradientLayer = CAGradientLayer()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupUI() {
        setupGradient()
        screenView.backgroundColor = .black
        addSubview(screenView)
        screenView.snp.makeConstraints { make in
            make.left.right.top.bottom.equalToSuperview().inset(50)
        }
        predictionLabel.textColor = .white
        predictionLabel.font = UIFont.systemFont(ofSize: 11, weight: .regular)
        predictionLabel.numberOfLines = 0
        predictionLabel.textAlignment = .center
        screenView.addSubview(predictionLabel)
        predictionLabel.snp.makeConstraints { make in
            make.center.equalTo(screenView.snp.center)
            make.top.right.left.bottom.equalTo(screenView).inset(5)
        }
    }

    func setupPrediction(text: String) {
        predictionLabel.text = text
    }

    func hidePredictionLabel(isHidden: Bool) {
        predictionLabel.isHidden = isHidden
    }

    func setupBackgroundScreenView(color: UIColor) {
        screenView.backgroundColor = color
    }

    func checkPredictionTextIsEmpty() -> Bool {
        guard let text = predictionLabel.text else { return true }
        if text.isEmpty {
            return true
        }
        return false
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.size.height / 2
        gradientLayer.cornerRadius = frame.size.height / 2
        gradientLayer.frame = bounds
        screenView.layer.cornerRadius = screenView.frame.size.height / 2
    }

    private func setupGradient() {
        layer.addSublayer(gradientLayer)
        gradientLayer.colors = [UIColor.black.cgColor, UIColor(named: .darkBlue).cgColor]
    }

}
