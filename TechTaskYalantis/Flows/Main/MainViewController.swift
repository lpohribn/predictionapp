//
//  MainViewController.swift
//  TechTaskYalantis
//
//  Created by Liudmila on 21.10.2021.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa

class MainViewController: UIViewController {

    private let magicBallView = MagicBallView()
    private let instructionLabel = UILabel()
    private let backgroundImageView = UIImageView()
    private let countOfShakeLabel = UILabel()
    private var isRequestEnded = false

    private let mainViewModel: MainViewModel

    private let counter: BehaviorRelay<Int>

    private let disposeBag = DisposeBag()

    init(mainViewModel: MainViewModel) {
        self.mainViewModel = mainViewModel
        self.counter = BehaviorRelay<Int>(value: self.mainViewModel.presentCurrentCountOfShake())
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupBindingCounter()
    }

    private func setupBindingCounter() {
        counter
            .map(String.init)
            .bind(to: countOfShakeLabel.rx.text)
            .disposed(by: disposeBag)

    }

    private func setupUI() {
        view.backgroundColor = UITraitCollection.current.userInterfaceStyle == .dark ? UIColor(named: .black) : UIColor(named: .white)
        setupNavigationBar()
        setupBackground()
        setupInstructionLabel()
        setupCounter()
        setupMagicBallView()
    }

    @objc func settingsBarItemPressed() {
        mainViewModel.goToSettings()
    }

    override func motionBegan(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        counter.accept(counter.value + 1)
        mainViewModel.updateCountOfShake(count: counter.value)
        instructionLabel.isHidden = true
        magicBallView.hidePredictionLabel(isHidden: true)
        self.magicBallView.setupBackgroundScreenView(color: .black)
    }

    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        instructionLabel.isHidden = false
        animateMagicBall()
        mainViewModel.updateViewPresenting()
            .subscribe(onNext: { [weak self] answer in
                self?.magicBallView.setupPrediction(text: answer ?? "NO")
                self?.mainViewModel.saveHistoryAnswer(answer: answer ?? "")
            })
            .disposed(by: disposeBag)
    }

    override func motionCancelled(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        instructionLabel.isHidden = false
    }
}

private extension MainViewController {

    func animateMagicBall() {
        let animator = UIViewPropertyAnimator(duration: 3, curve: .easeInOut) {
            self.magicBallView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.magicBallView.setupBackgroundScreenView(color: UIColor(named: .lightPurple))
        }
        animator.addAnimations({
            self.magicBallView.transform = CGAffineTransform.identity
        }, delayFactor: 0.5)

        animator.addCompletion { _ in
            if self.magicBallView.checkPredictionTextIsEmpty() == true {
                self.animateMagicBall()
            } else {
                self.magicBallView.hidePredictionLabel(isHidden: false)
            }
        }
        animator.startAnimation()
    }

    func setupNavigationBar() {
        if let navBar = navigationController?.navigationBar {
            navBar.scrollEdgeAppearance = navBar.standardAppearance
            navigationItem.title = "Main"
            navBar.backgroundColor = UITraitCollection.current.userInterfaceStyle == .dark ? UIColor(named: .black) : UIColor(named: .white)
            navBar.titleTextAttributes = UITraitCollection.current.userInterfaceStyle == .dark ? [.foregroundColor: UIColor(named: .white)] :
            [.foregroundColor: UIColor(named: .black)]
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "gear"),
                                                                style: .plain,
                                                                target: self,
                                                                action: #selector(settingsBarItemPressed))
        }
    }

    func setupBackground() {
        backgroundImageView.image = Asset.nightSkyPines.image
        view.addSubview(backgroundImageView)
        backgroundImageView.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide)
            make.left.right.equalToSuperview()
            make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
        }
    }

    func setupInstructionLabel() {
        instructionLabel.text = L10n.instruction
        instructionLabel.textAlignment = .center
        instructionLabel.textColor = .white
        instructionLabel.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
        view.addSubview(instructionLabel)
        instructionLabel.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide).inset(40)
            make.centerX.equalTo(view.safeAreaLayoutGuide.snp.centerX)
        }
    }

    func setupCounter() {
        countOfShakeLabel.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
        countOfShakeLabel.textColor = UIColor(named: .white)
        countOfShakeLabel.numberOfLines = 0
        view.addSubview(countOfShakeLabel)
        countOfShakeLabel.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide).inset(5)
            make.left.right.equalTo(view.safeAreaLayoutGuide).inset(10)
            make.bottom.greaterThanOrEqualTo(instructionLabel.snp.top).inset(-5)
        }
    }

    func setupMagicBallView() {
        view.addSubview(magicBallView)
        magicBallView.snp.makeConstraints { make in
            make.top.greaterThanOrEqualTo(instructionLabel.snp.bottom).inset(-10)
            make.center.equalTo(view.safeAreaLayoutGuide.snp.center)
            make.height.width.equalTo(200)
        }
    }

}
