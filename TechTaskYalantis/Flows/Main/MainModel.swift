//
//  MainModel.swift
//  TechTaskYalantis
//
//  Created by Liudmila on 14.11.2021.
//

import Foundation
import RxSwift

class MainModel: NavigationNode {
    private let networkDataProvider: NetworkDataProvider
    private let dataBaseProvider: DataBaseProvider
    private let secureProvider: SecureProvider
    private let disposeBag = DisposeBag()

    init(parent: NavigationNode, networkDataProvider: NetworkDataProvider, dataBaseProvider: DataBaseProvider, secureProvider: SecureProvider) {
        self.networkDataProvider = networkDataProvider
        self.dataBaseProvider = dataBaseProvider
        self.secureProvider = secureProvider
        super.init(parent: parent)
    }

    func fetchAnswer() -> Observable<String?> {
        return Observable.create { [weak self] observer in
            self?.networkDataProvider.performRequest(urlString: L10n.getAnswerUrl)
                .subscribe { answer in
                    if let answer = answer {
                        observer.on(.next(answer))
                    }
                } onError: { _ in
                    observer.on(.next(self?.dataBaseProvider.getAnswers(isLocal: true).randomElement()?.answer))
                }.disposed(by: self?.disposeBag ?? DisposeBag())
            return Disposables.create()
        }
    }

    func saveCountOfShake(count: Int) {
        secureProvider.setValue(setValue: count, forKey: StorageKey.keyForCounterOfShake.rawValue)
    }

    func getCountOfShake() -> Int {
        return secureProvider.getValue(forKey: StorageKey.keyForCounterOfShake.rawValue)
    }

    func saveAnswer(answer: String) {
        dataBaseProvider.saveAnswer(answer: Answer(answer: answer, date: Date(), isLocal: false))
    }

    func goToSettings() {
        raise(event: MainScreenEvent.settings)
    }

}
