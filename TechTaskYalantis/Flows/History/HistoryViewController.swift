//
//  HistoryViewController.swift
//  TechTaskYalantis
//
//  Created by Liudmila on 25.11.2021.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa

class HistoryViewController: UITableViewController {

    private let historyViewModel: HistoryViewModel

    init(historyViewModel: HistoryViewModel) {
        self.historyViewModel = historyViewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        historyViewModel.updateHistoryAnswers()
        tableView.reloadData()
    }

    func setupUI() {
        setupNavigationBar()
        setTabBar()
        setAppearanceForTableView()
    }

    private func setupNavigationBar() {
        if let navBar = navigationController?.navigationBar {
            navBar.scrollEdgeAppearance = navBar.standardAppearance
            navigationItem.title = "History"
            navBar.backgroundColor = UITraitCollection.current.userInterfaceStyle == .dark ? UIColor(named: .black) : UIColor(named: .white)
            navBar.titleTextAttributes = UITraitCollection.current.userInterfaceStyle == .dark ? [.foregroundColor: UIColor(named: .white)] :
            [.foregroundColor: UIColor(named: .black)]
        }
    }

    private func setTabBar() {
        if let tabBar = tabBarController?.tabBar {
            tabBar.backgroundColor = .white
        }
    }

    private func setAppearanceForTableView() {
        tableView.separatorStyle = .singleLine
        tableView.backgroundColor = .black
        tableView.estimatedRowHeight = 200
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(HistoryCellTableViewCell.self, forCellReuseIdentifier: HistoryCellTableViewCell.identifier)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyViewModel.historyAnswers.count
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if let cell = tableView.dequeueReusableCell(withIdentifier: HistoryCellTableViewCell.identifier) as? HistoryCellTableViewCell {
            cell.setupHistoryData(answer: historyViewModel.historyAnswers[indexPath.row].answer,
                                  date: historyViewModel.getDateString(date: historyViewModel.historyAnswers[indexPath.row].date))
            cell.selectionStyle = .none
            return cell
        }
        return  UITableViewCell()
    }
    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
          return .delete
      }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
            if editingStyle == .delete {
                historyViewModel.deleteHistoryAnswer(answer: historyViewModel.historyAnswers[indexPath.row])
                historyViewModel.historyAnswers.remove(at: indexPath.row)
                    tableView.deleteRows(at: [indexPath], with: .fade)
                    tableView.reloadData()
                }
        }
}
