//
//  HistoryModel.swift
//  TechTaskYalantis
//
//  Created by Liudmila on 26.11.2021.
//

import Foundation

class HistoryModel {

    private let dataBaseProvider: DataBaseProvider

    init(dataBaseProvider: DataBaseProvider) {
        self.dataBaseProvider = dataBaseProvider
    }

    func getHistoryAnswers(isLocal: Bool) -> [Answer] {
        return dataBaseProvider.getAnswers(isLocal: isLocal)
    }

    func deleteHistoryAnswer(answer: Answer) {
        dataBaseProvider.deleteAnswer(answer: answer)
    }
}
