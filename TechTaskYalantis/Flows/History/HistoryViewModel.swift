//
//  HistoryViewModel.swift
//  TechTaskYalantis
//
//  Created by Liudmila on 26.11.2021.
//

import Foundation

class HistoryViewModel {

    private let historyModel: HistoryModel
    var historyAnswers: [Answer] = []

    init(historyModel: HistoryModel) {
        self.historyModel = historyModel
    }

    func getDateString(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
        return dateFormatter.string(from: date)
    }

    func deleteHistoryAnswer(answer: Answer) {
        historyModel.deleteHistoryAnswer(answer: answer)
    }

    func updateHistoryAnswers() {
        let rlmHistoryAnswers = historyModel.getHistoryAnswers(isLocal: false)
        historyAnswers.removeAll()
        for answer in rlmHistoryAnswers {
            historyAnswers.append(Answer(answer: answer.answer, date: answer.date, isLocal: answer.isLocal))
        }
    }
}
