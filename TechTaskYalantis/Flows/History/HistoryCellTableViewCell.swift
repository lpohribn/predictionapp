//
//  HistoryCellTableViewCell.swift
//  TechTaskYalantis
//
//  Created by Liudmila on 26.11.2021.
//

import UIKit
import SnapKit

class HistoryCellTableViewCell: UITableViewCell {

    private let storedAnswerLabel = UILabel()
    private let dateLabel = UILabel()

    static let identifier = "historyCellIdentifier"

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = UIColor(named: .lightPurple)
        setupUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupUI() {
        contentView.addSubview(dateLabel)
        dateLabel.numberOfLines = 0
        dateLabel.textColor = .white
        dateLabel.font = UIFont.systemFont(ofSize: 15)
        dateLabel.snp.makeConstraints { make in
            make.top.equalTo(contentView.snp.top).inset(10)
            make.bottom.equalTo(contentView.snp.bottom).inset(10)
            make.right.equalTo(contentView.snp.right).inset(20)
        }

        contentView.addSubview(storedAnswerLabel)
        storedAnswerLabel.numberOfLines = 0
        storedAnswerLabel.textColor = .white
        storedAnswerLabel.font = UIFont.systemFont(ofSize: 15)
        storedAnswerLabel.snp.makeConstraints { make in
            make.top.equalTo(contentView.snp.top).inset(10)
            make.bottom.equalTo(contentView.snp.bottom).inset(10)
            make.left.equalTo(contentView.snp.left).inset(20)
            make.right.equalTo(contentView.snp.centerX).inset(20)
        }
    }

    func setupHistoryData(answer: String, date: String) {
        storedAnswerLabel.text = answer
        dateLabel.text = date
    }
}
