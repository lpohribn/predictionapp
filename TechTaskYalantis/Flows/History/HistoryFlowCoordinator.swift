//
//  HistoryFlowCoordinator.swift
//  TechTaskYalantis
//
//  Created by Liudmila on 19.12.2021.
//

import UIKit

public class HistoryFlowCoordinator: NavigationNode, FlowCoordinator {

    var containerViewController: UIViewController?
    private let dataBaseService: DataBaseService

    init(parent: NavigationNode?, dataBaseService: DataBaseService) {
        self.dataBaseService = dataBaseService
        super.init(parent: parent)
    }

    func createFlow() -> UIViewController {
        let dataBaseService = DataBaseService()
        let historyModel = HistoryModel(dataBaseProvider: dataBaseService)
        let historyViewModel = HistoryViewModel(historyModel: historyModel)
        containerViewController = HistoryViewController(historyViewModel: historyViewModel)
        return UINavigationController(rootViewController: containerViewController ?? UIViewController())
    }

}
