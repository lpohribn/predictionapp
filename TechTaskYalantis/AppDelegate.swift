//
//  AppDelegate.swift
//  TechTaskYalantis
//
//  Created by Liudmila on 21.10.2021.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var appFlowNavigator: AppFlowNavigator?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        appFlowNavigator = AppFlowNavigator(window: window!)
        appFlowNavigator?.startFlow()
        window?.makeKeyAndVisible()
        return true
    }

}
