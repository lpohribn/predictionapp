// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name vertical_whitespace_opening_braces
internal enum L10n {
  /// https://8ball.delegator.com/magic/JSON/yes?
  internal static let getAnswerUrl = L10n.tr("Localizable", "getAnswerUrl")
  /// Ask question and shake
  internal static let instruction = L10n.tr("Localizable", "instruction")
  /// Maybe
  internal static let maybe = L10n.tr("Localizable", "maybe")
  /// Never
  internal static let never = L10n.tr("Localizable", "never")
  /// No
  internal static let no = L10n.tr("Localizable", "no")
  /// No doubt
  internal static let noDoubt = L10n.tr("Localizable", "noDoubt")
  /// Saved
  internal static let save = L10n.tr("Localizable", "save")
  /// Save answer
  internal static let saveButtonText = L10n.tr("Localizable", "saveButtonText")
  /// Your answer
  internal static let settingsLabel = L10n.tr("Localizable", "settingsLabel")
  /// Enter your variant
  internal static let titleForPlaceHolder = L10n.tr("Localizable", "titleForPlaceHolder")
  /// Fill a field
  internal static let warningForField = L10n.tr("Localizable", "warningForField")
  /// Yes
  internal static let yes = L10n.tr("Localizable", "yes")
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name vertical_whitespace_opening_braces

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = BundleToken.bundle.localizedString(forKey: key, value: nil, table: table)
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
