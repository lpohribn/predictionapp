//
//  Answer.swift
//  TechTaskYalantis
//
//  Created by Liudmila on 28.11.2021.
//

import Foundation

struct Answer {
    let answer: String
    let date: Date
    let isLocal: Bool
}

extension Answer {
    func toRlmObject() -> RLMHistoryAnswers {
        let historyAnswersObject = RLMHistoryAnswers()
        historyAnswersObject.answer = answer
        historyAnswersObject.date = date
        historyAnswersObject.isLocal = isLocal
        return historyAnswersObject
    }
}
