//
//  RLMHistoryAnswersData.swift
//  TechTaskYalantis
//
//  Created by Liudmila on 26.11.2021.
//

import Foundation
import RealmSwift

class RLMHistoryAnswers: Object {
    @objc dynamic var answer: String = "No"
    @objc dynamic var date: Date = Date()
    @objc dynamic var isLocal: Bool = true
}

extension RLMHistoryAnswers {
    func toAnswers() -> Answer {
        return Answer(answer: answer, date: date, isLocal: isLocal)
    }
}
