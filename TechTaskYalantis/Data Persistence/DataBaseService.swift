//
//  DataBaseService.swift
//  TechTaskYalantis
//
//  Created by Liudmila on 08.11.2021.
//

import Foundation
import RealmSwift

protocol DataBaseProvider {
    func getAnswers(isLocal: Bool) -> [Answer]
    func saveAnswer(answer: Answer)
    func deleteAnswer(answer: Answer)
}

class DataBaseService: DataBaseProvider {

    func getAnswers(isLocal: Bool) -> [Answer] {
        var historyAnswers: [Answer] = []
        let backgroundQueue = DispatchQueue(label: "getAnswerInBackground", qos: .background)
        backgroundQueue.sync {
            let realm = try? Realm()
            realm?.refresh()
            if let objects = realm?.objects(RLMHistoryAnswers.self).sorted(byKeyPath: "date", ascending: false) {
                historyAnswers.append(contentsOf: objects.filter {$0.isLocal == isLocal}.map {$0.toAnswers() })
            }
        }
        return historyAnswers
    }

    func saveAnswer(answer: Answer) {
        let backgroundQueue = DispatchQueue(label: "saveAnswerInBackground", qos: .background)
        backgroundQueue.async {
            let realm = try? Realm()
            realm?.refresh()
            do {
                try realm?.write {
                    realm?.add(answer.toRlmObject())
                }
            } catch {
                print("error")
            }
        }
    }

    func deleteAnswer(answer: Answer) {
        let backgroundQueue = DispatchQueue(label: "deleteAnswerInBackground", qos: .background)
        backgroundQueue.async {
            let realm = try? Realm()
            realm?.refresh()
            do {
                try realm?.write {
                    if let object = realm?.objects(RLMHistoryAnswers.self).filter({$0.date == answer.date}) {
                        realm?.delete(object)
                    }
                }
            } catch {
                print("error")
            }
        }
    }

}
