//
//  SecureStorage.swift
//  TechTaskYalantis
//
//  Created by Liudmila on 22.11.2021.
//

import Foundation
import SwiftKeychainWrapper

protocol SecureProvider {
    func setValue(setValue: Int, forKey: String)
    func getValue(forKey: String) -> Int
}

class SecureStorage: SecureProvider {

    func setValue(setValue: Int, forKey: String) {
        KeychainWrapper.standard.set(setValue, forKey: forKey)
    }

    func getValue(forKey: String) -> Int {
        return KeychainWrapper.standard.integer(forKey: forKey) ?? 0
    }

}

enum StorageKey: String {
    case keyForCounterOfShake = "numberOfShake"
}
