//
//  NetworkingManager.swift
//  TechTaskYalantis
//
//  Created by Liudmila on 21.10.2021.
//

import Foundation
import RxSwift

protocol NetworkDataProvider {
    func performRequest(urlString: String) -> Observable<String?>
}

class NetworkingManager: NetworkDataProvider {
    private let disposeBag = DisposeBag()

    private enum Error: Swift.Error {
        case invalidResponse(URLResponse)
        case invalidJSON(Swift.Error)
    }
    func performRequest(urlString: String) -> Observable<String?> {
        guard let url = URL(string: urlString) else {
            return  Observable.create { observer in
                observer.onNext("")
                return Disposables.create()
            }
        }
        return Observable.create { observer in
            let request = URLRequest(url: url)
            URLSession.shared.rx.response(request: request)
                .observe(on: MainScheduler.asyncInstance)
                .subscribe(onNext: { response in
                    do {
                        let currentAnswerData = try JSONDecoder().decode(MagicBallAnswerData.self, from: response.data)
                        observer.onNext(currentAnswerData.magic.answer)
                    } catch let error as NSError {
                        observer.onError(error)
                    }
                }, onError: { error in
                    observer.onError(error)
                }).disposed(by: self.disposeBag)
            return Disposables.create()
        }
    }
}
