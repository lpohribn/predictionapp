//
//  MagicBallAnswerData.swift
//  TechTaskYalantis
//
//  Created by Liudmila on 22.10.2021.
//

import Foundation

struct MagicBallAnswerData: Decodable {
    let magic: Magic
}

struct Magic: Decodable {
    let answer: String
}
