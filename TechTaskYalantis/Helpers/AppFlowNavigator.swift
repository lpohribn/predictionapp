//
//  AppFlowNavigator.swift
//  TechTaskYalantis
//
//  Created by Liudmila on 19.12.2021.
//

import UIKit

final class AppFlowNavigator: NavigationNode {

    private let window: UIWindow

    init(window: UIWindow) {
        self.window = window
        super.init(parent: nil)
    }

    func startFlow() {
            let coordinator = TabBarFlowCoordinator(parent: self)
            let controller = coordinator.createFlow()
            window.rootViewController = controller
            window.makeKeyAndVisible()
    }

}
