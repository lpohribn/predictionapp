//
//  FlowCoordinator.swift
//  TechTaskYalantis
//
//  Created by Liudmila on 19.12.2021.
//

import Foundation
import UIKit

protocol FlowCoordinator {

    var containerViewController: UIViewController? { get set }

    func createFlow() -> UIViewController

}
