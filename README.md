# **MagicBallApp**

## Description
This project is a technical task for Yalantis school. It is a game that simulates a magic ball with predictions. https://ru.wikipedia.org/wiki/Magic_8_ball.

There is tabbar with two screens: the first one(main screen) - animating ball with prediction text, another(history screen) - the list of answers on previous questions.

There is also settings screen, where you can add a custom answer.

![](magicBall.gif)

## How to use
 All you need - ask question, shake device and wait for the answer.
 
## Basic information
- MVVM architecture
- Database is Realm
- Layout created with SnapKit
- RxSwift, Swiftgen, SwiftLint installed and used in project.
 
